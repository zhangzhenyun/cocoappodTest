Pod::Spec.new do |s|
s.name             = "cocoappodTest"
s.version          = "1.0.0"
s.summary          = "Just Testing."
s.homepage         = "https://gitlab.com/zhangzhenyun/cocoappodTest"
s.license          = 'MIT'
s.author           = { "zhangzhenyun" => "zhangzhenyun@techfit.cn" }
s.source           = { :git => "https://gitlab.com/zhangzhenyun/cocoappodTest.git", :tag => "1.0.0" }
s.platform     = :ios, '7.0'
s.requires_arc = true
s.source_files = 'Demo/**/*'
s.public_header_files = 'Demo/**/*.h'
s.frameworks = 'UIKit'
end
